﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CosmoDB.Console.Model;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace CosmoDB.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //var key = "EtYGuhmm0iTDZYXAaeLSBGamfFiaaPzZPGLYiHlGTkzdrZFGhmkmSPvVXuxYbzA1SWSxxmg3VEOYzJnMIoxVgw==";
            //var uri = "https://mindcept-cosmo.documents.azure.com:443/";


            // Create a new instance of the Cosmos Client
            using (var client = new CosmosClient("AccountEndpoint=https://ekizer-mindcept-cosmo.documents.azure.com:443/;AccountKey=CSsskkJIMyCa2r2NNpuGRMhd78dmPj45IVJrzin4t27dh0osY7mSMfqf0dwCR7WEbP9QMMEm7FcMpHvytM2bKA==;;"))
            {
                //var DB = client.GetDatabase("ChampionProductCosmoDB");
                //var table = client.GetContainer(DB.Id, "Customer");

                ////loop through all records
                //var sqlString = "Select * from c";
                //var query = new QueryDefinition(sqlString);
                //FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                //FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                //foreach (var item in queryResult.Resource)
                //{
                //    Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                //}


                ////Get records with 'Smith' as lastname
                //sqlString = "SELECT * FROM c where c.lastName = 'Smith'";
                //query = new QueryDefinition(sqlString);
                //iterator = table.GetItemQueryIterator<Customer>(sqlString);

                //queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                //foreach (var item in queryResult.Resource)
                //{
                //    Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                //}


                //////CRUD examples
                ConnectionPolicy connectionPolicy = new ConnectionPolicy();
                connectionPolicy.ConnectionMode = Microsoft.Azure.Documents.Client.ConnectionMode.Direct;
                connectionPolicy.ConnectionProtocol = Protocol.Tcp;

                // Set the read region selection preference order
                connectionPolicy.PreferredLocations.Add(LocationNames.EastUS); // first preference
                connectionPolicy.PreferredLocations.Add(LocationNames.NorthEurope); // second preference
                connectionPolicy.PreferredLocations.Add(LocationNames.SoutheastAsia); // third preference

                var documentClient = new DocumentClient(new Uri("https://ekizer-mindcept-cosmo.documents.azure.com:443/"), "CSsskkJIMyCa2r2NNpuGRMhd78dmPj45IVJrzin4t27dh0osY7mSMfqf0dwCR7WEbP9QMMEm7FcMpHvytM2bKA==", connectionPolicy);
                var Service = new CosmoDB.Console.Service.CustomerService();

                //Add a new customer
                var newCustomer = new Customer()
                {
                    customerId = 1,
                    lastName = "Smith",
                    firstName = "Tabb",
                    addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=1,
                                                    addressTypeId = 1,
                                                    street1 = "100 Main Street",
                                                    street2 = "Suite 2",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    },
                                                new Address()
                                                {
                                                    addressId=2,
                                                    addressTypeId = 2,
                                                    street1 = "P.O. Box 123",
                                                    street2 = null,
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75202"
                                                }
                },
                    invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 1,
                                                    date = DateTime.Parse("2019-04-28"),
                                                    amount = decimal.Parse("101.25"),
                                                    paidDate = null,
                                                    paidAmount = 0,
                                                },
                                                new Invoice()
                                                {
                                                    invoiceId = 2,
                                                    date = DateTime.Parse("2019-05-01"),
                                                    amount = decimal.Parse("23.54"),
                                                    paidDate = DateTime.Parse("2019-05-02"),
                                                    paidAmount = decimal.Parse("23.54")
                                                }

                }
                };

                var wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer)).GetAwaiter().GetResult();

                //Add another customer
                var newCustomer2 = new Customer()
                {
                    customerId = 2,
                    lastName = "Jones",
                    firstName = "Tammy",
                    addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=3,
                                                    addressTypeId = 1,
                                                    street1 = "111 Main Street",
                                                    street2 = "Suite 25",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    },
                                                new Address()
                                                {
                                                    addressId=4,
                                                    addressTypeId = 2,
                                                    street1 = "P.O. Box 123445",
                                                    street2 = null,
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75202"
                                                }
                },
                    invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 3,
                                                    date = DateTime.Parse("2019-02-28"),
                                                    amount = decimal.Parse("121.25"),
                                                    paidDate = null,
                                                    paidAmount = 10,
                                                },
                                                new Invoice()
                                                {
                                                    invoiceId = 4,
                                                    date = DateTime.Parse("2019-04-01"),
                                                    amount = decimal.Parse("223.54"),
                                                    paidDate = DateTime.Parse("2019-05-02"),
                                                    paidAmount = decimal.Parse("223.54")
                                                }

                }
                };

                wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer2)).GetAwaiter().GetResult();

                //Add another customer
                var newCustomer3 = new Customer()
                {
                    customerId = 3,
                    lastName = "Struck",
                    firstName = "Charles",
                    addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=5,
                                                    addressTypeId = 1,
                                                    street1 = "3000 Lewis Drive",
                                                    street2 = "Suite 200",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    }

                },
                    invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 5,
                                                    date = DateTime.Parse("2019-02-20"),
                                                    amount = decimal.Parse("1121.25"),
                                                    paidDate = null,
                                                    paidAmount = decimal.Parse("100.22"),
                                                }

                }
                };

                wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer3)).GetAwaiter().GetResult();


                //Update a customer
                //getting the customer I want to update.
                Customer customerToUpdate = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductCosmoDB", "Customer2"))
                .Where(c => c.id == "2c1c0c87-e408-4167-99ef-5ff2eadb4f0a")
                .AsEnumerable()
                .FirstOrDefault();

                if (customerToUpdate != null)
                {
                    customerToUpdate.firstName = "Peter"; //make a change to any property in the customer object
                    customerToUpdate.addresses.FirstOrDefault().state = "MN"; //make a change to any property in the customer object

                    var wasSuccessfullyUpdated = Task.Run(() => Service.UpdateCustomerAsync(documentClient, customerToUpdate)).GetAwaiter().GetResult();
                }


                //delete a customer
                //getting the customer I want to delete.
                Customer customerToDelete = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductCosmoDB", "Customer2"))
                .Where(c => c.id == "2c1c0c87-e408-4167-99ef-5ff2eadb4f0a")
                .AsEnumerable()
                .LastOrDefault();

                if (customerToDelete != null)
                {
                    var wasSuccessfullyDeleted = Task.Run(() => Service.DeleteCustomerAsync(documentClient, customerToDelete)).GetAwaiter().GetResult();
                }

            }


        }




    }
}